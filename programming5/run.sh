#!/bin/bash

ABODYFILE="data/Problem5-BodyA.txt"
BBODYFILE="data/Problem5-BodyB.txt"
MESHFILE="data/Problem5MeshFile.sur"
SAMPLEFILE="data/PA5-A-Debug-SampleReadingsTest.txt"
MODEFILE="data/Problem5Modes.txt"
OUTFILE="data/PA5-A-Debug-Output.txt"

python3 main.py $ABODYFILE $BBODYFILE $MESHFILE $SAMPLEFILE $MODEFILE $OUTFILE
