import sys
import numpy as np

def read_body_file(fn):
    f = open(fn)

    # Read number of markers
    n_markers = int(f.readline().split()[0])

    # Read coords of LED markers in body coords
    LED_markers = np.zeros((n_markers, 3))
    for i in range(n_markers):
        LED_markers[i] = np.array(f.readline().split(), dtype=float)

    # Read tip coords in body coords
    tip_coords = np.array(f.readline().split(), dtype=float)
       
    return n_markers, LED_markers, tip_coords


def read_mesh_file(fn):
    f = open(fn)

    # Read number of vertices
    n_vertices = int(f.readline().split()[0])

    # Read vertices in CT coords
    vertices = np.zeros((n_vertices, 3))
    for i in range(n_vertices):
        vertices[i] = np.array(f.readline().split(), dtype=float)

    # Read number of triangles
    n_triangles = int(f.readline().split()[0])
    
    # Read vertex indices for triangles
    triangles = np.zeros((n_triangles, 3), dtype=int)
    for i in range(n_triangles):
        triangles[i] = np.array(f.readline().split()[0:3], dtype=int)

    return vertices, triangles

def read_sample_readings(fn, Na, Nb):
    f = open(fn)

    tokens = f.readline().split(",")
    Ns, Nframes = int(tokens[0]), int(tokens[1])

    Nd = Ns - Na - Nb

    A_tracker = np.zeros((Nframes, Na, 3))
    B_tracker = np.zeros((Nframes, Nb, 3))
    D_tracker = np.zeros((Nframes, Nd, 3))

    # Read fr frames of LED tracker data
    for fr in range(Nframes):

        # Read A LED trackers
        A_frame = np.zeros((Na, 3))
        for i in range(Na):
            A_frame[i] = np.array(f.readline().split(","), dtype=float)

        # Read B LED trackers
        B_frame = np.zeros((Nb, 3))
        for i in range(Nb):
            B_frame[i] = np.array(f.readline().split(","), dtype=float)

        # Read Dummy trackers
        D_frame = np.zeros((Nd, 3))
        for i in range(Nd):
            D_frame[i] = np.array(f.readline().split(","), dtype=float)

        A_tracker[fr] = A_frame
        B_tracker[fr] = B_frame
        D_tracker[fr] = D_frame
    
    return A_tracker, B_tracker, D_tracker

def read_output(fn):
    f = open(fn)
    Nframes = int(f.readline().split()[0])    
    d = np.zeros((Nframes, 3))
    c = np.zeros((Nframes, 3))
    dist = np.zeros(Nframes)
    for fr in range(Nframes):
        tokens = np.array(f.readline().split(), dtype=float)
        d[fr], c[fr], dist[fr] = tokens[0:3], tokens[3:6], tokens[6]

    return d, c, dist

def read_modes(fn):
    f = open(fn)

    header = f.readline().split()
    Nvertices = int(header[1].split("=")[1])
    Nmodes = int(header[2].split("=")[1]) + 1

    mode_vtx = np.zeros((Nmodes, Nvertices, 3), dtype=float)
    for i in range(Nmodes):
        comment = f.readline()
        for j in range(Nvertices):
            mode_vtx[i, j] = np.array(f.readline().split(","), dtype=float)

    return mode_vtx

def print_output(fn, numframes, d, c, dist, mode_weights):
    f = open(fn, "w")
    
    f.write(str(numframes) + " " + fn + "\n")
    
    f.write(str(mode_weights).strip('[]') + "\n")

    for k in range(numframes):
        
        
        d_string = str(d[k]).strip('[]')
         
        c_string = str(c[k]).strip('[]')
        
        d_outputs = d_string.split()
        c_outputs = c_string.split()
        outstring = "{:>8}{:>9}{:>9}{:>13}{:>9}{:>9}{:>10}\n".format(d_outputs[0],d_outputs[1],d_outputs[2],c_outputs[0],c_outputs[1],c_outputs[2], str(dist[k]))
        f.write(outstring)
    f.close()
