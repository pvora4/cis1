#!/bin/python3

"""
Parth Vora & Miguel Inserni
CIS I PA4
11/21/2019
"""

import sys
import numpy as np

import bounding_spheres as bsph

def calculates_center():

    # Correctly calculates center
    p1 = np.array((0, 0, 0), dtype=float)
    p2 = np.array((0, 2, 0), dtype=float)
    p3 = np.array((1, 1, 0), dtype=float)
    center = np.array((0, 1, 0), dtype=float)

    s = bsph.sphere(p1, p2, p3, None)
    assert(np.array_equal(s.q, center))

def longest_edge_found():

    # Longest edge will always be p2 - p3
    p1 = np.array((0, 0, 0), dtype=float)
    p2 = np.array((1, 0, 0), dtype=float)
    p3 = np.array((0, 2, 0), dtype=float)

    s1 = bsph.sphere(p1, p2, p3, None)
    assert(np.array_equal(s1.a, p2))
    assert(np.array_equal(s1.b, p3))

    s2 = bsph.sphere(p1, p3, p2, None)
    assert(np.array_equal(s2.a, p3))
    assert(np.array_equal(s2.b, p2))

    s3 = bsph.sphere(p2, p1, p3, None)
    assert(np.array_equal(s3.a, p2))
    assert(np.array_equal(s3.b, p3))

    s4 = bsph.sphere(p2, p3, p1, None)
    assert(np.array_equal(s4.a, p2))
    assert(np.array_equal(s4.b, p3))

    s5 = bsph.sphere(p3, p1, p2, None)
    assert(np.array_equal(s5.a, p3))
    assert(np.array_equal(s5.b, p2))

    s6 = bsph.sphere(p3, p2, p1, None)
    assert(np.array_equal(s6.a, p3))
    assert(np.array_equal(s6.b, p2))

def main(argv):

    longest_edge_found()
    calculates_center()

if __name__ == "__main__":
    main(sys.argv[1:])
