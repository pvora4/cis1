"""
Parth Vora & Miguel Inserni
CIS I PA4
"""

import sys
import numpy as np
import triangles as tg

"""
Class defining a bounding sphere around a triangle.
Input: 3 points (np array 3x3), modes (
Output: Bounding sphere around a triangle object
"""
class sphere:
    def __init__(self, _p1, _p2, _p3, _modes):

        self.triangle = tg.triangle(_p1, _p2, _p3)

        # a-b is the longest edge
        self.a, self.b, self.c = self.get_largest_edge()
        
        # Center
        self.q = self.compute_center()

        # Radius
        self.r = np.linalg.norm(self.q - self.a)
        
        # Modes
        self.modes = _modes

    """
    Function that finds the largest edge of the triangle
    Input: none
    Output: points in order of largest edge
    """
    def get_largest_edge(self):
       
        # p2 - p1
        n1 = np.linalg.norm(self.triangle.v1)
        
        # p3 - p1
        n2 = np.linalg.norm(self.triangle.v2)
        
        # p3 - p2
        n3 = np.linalg.norm(self.triangle.v3)

        if n1 > n2 and n1 > n3:
            return self.triangle.p1, self.triangle.p2, self.triangle.p3

        elif n2 > n1 and n2 > n3:
            return self.triangle.p1, self.triangle.p3, self.triangle.p2

        return self.triangle.p2, self.triangle.p3, self.triangle.p1

    """
    Function that computes center of bounding sphere
    Input: none
    Output: center of bounding sphere
    """
    def compute_center(self):
        
        f = (self.a + self.b) / 2
        u = self.a - f
        v = self.c - f
        d = np.cross(np.cross(u, v), u)

        gamma = np.sum(np.square(v) - np.square(u))
        gamma = np.linalg.norm(v) - np.linalg.norm(u)
        gamma /= (np.dot(d, (v - u)))
            
        if gamma > 0:
            f += (gamma * d)

        return f
    
    """
    Function that calls closest point on triangle
    Input: point (3x1 np array)
    Output: closest point (3x1 np array)
    """
    def closest_point(self, p_orig):
        return self.triangle.closest_point(p_orig)

    """
    Function that calls triangle class compute barycentric coords.
    Input: point (3x1 np array)
    Output: barycentric coordinates u, v, w
    """
    def compute_barycentric_coords(self, p_orig):
        return self.triangle.compute_barycentric_coords(p_orig)
