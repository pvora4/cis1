#!/bin/python3

"""
Parth Vora & Miguel Inserni
CIS I PA3
11/21/2019
"""

import numpy as np
import sys

import IO
import transformations as tf
import triangles as tg

def barycentric_coords_accurate():

    a = np.array((0, 0, 0), dtype=float)
    b = np.array((0, 1, 0), dtype=float)
    c = np.array((1, 0, 0), dtype=float)
    t = tg.triangle(a, b, c)

    u, v, w = t.compute_barycentric_coords(a)
    p = t.p1 + u * (b-a) + v * (c-a) 
    assert(np.array_equal(p, a))

def outside_point_closest_to_edge():

    a = np.array((0, 0, 0), dtype=float)
    b = np.array((0, 1, 0), dtype=float)
    c = np.array((1, 0, 0), dtype=float)
    t = tg.triangle(a, b, c)

    p = np.array((-0.5, 0.5, 0), dtype=float)
    close_p = t.closest_point(p)
    proj = np.array((0, 0.5, 0), dtype=float)
    assert((close_p == proj).all())

    p = np.array((0.5, -0.5, 0), dtype=float)
    close_p = t.closest_point(p)
    proj = np.array((0.5, 0, 0), dtype=float)
    assert((close_p == proj).all())

    p = np.array((1, 1, 0), dtype=float)
    close_p = t.closest_point(p)
    close_p = np.round(close_p, 10)
    proj = np.array((0.5, 0.5, 0), dtype=float)
    assert((close_p == proj).all())

def outside_point_closest_to_vertex():

    a = np.array((0, 0, 0), dtype=float)
    b = np.array((0, 1, 0), dtype=float)
    c = np.array((1, 0, 0), dtype=float)
    t = tg.triangle(a, b, c)

    p = np.array((-1, -1, 0), dtype=float)
    close_p = t.closest_point(p)
    assert((a == close_p).all())

    p = np.array((0, 2, 0), dtype=float)
    close_p = t.closest_point(p)
    assert((b == close_p).all())

    p = np.array((2, 0, 0), dtype=float)
    close_p = t.closest_point(p)
    assert((c == close_p).all())

def point_in_triangle():

    a = np.array((0, 0, 0), dtype=float)
    b = np.array((0, 1, 0), dtype=float)
    c = np.array((1, 0, 0), dtype=float)
    t = tg.triangle(a, b, c)

    p = np.array((0.5, 0.5, 0), dtype=float)
    close_p = t.closest_point(p)
    assert((p == close_p).all())

def point_is_vertex():

    a = np.array((0, 0, 0), dtype=float)
    b = np.array((0, 1, 0), dtype=float)
    c = np.array((1, 0, 0), dtype=float)
    t = tg.triangle(a, b, c)

    p = np.array((0, 0, 0), dtype=float)
    close_p = t.closest_point(p)
    assert((p == close_p).all())

    p = np.array((0, 1, 0), dtype=float)
    close_p = t.closest_point(p)
    assert((p == close_p).all())

    p = np.array((1, 0, 0), dtype=float)
    close_p = t.closest_point(p)
    assert((p == close_p).all())

def valid_triangles():

    # acute
    a = np.array((0, 0, 0))
    b = np.array((0, 1, 0))
    c = np.array((0.5, 0.5, 1))
    acute_triangle = tg.triangle(a, b, c)

    # right
    a = np.array((0, 0, 0))
    b = np.array((0, 1, 0))
    c = np.array((1, 0, 0))
    right_triangle = tg.triangle(a, b, c)

    # obtuse
    a = np.array((-1, 0.5, 0))
    b = np.array((0, 0, 0))
    c = np.array((0, 1, 0))
    obtuse_triangle = tg.triangle(a, b, c)

def degenerate_triangles():
    
    # triangle with all overlapping coords
    a = np.ones(3)
    
    try:
        point = tg.triangle(a, a, a)
        assert(False)
    except Exception:
        x = 0        

    # triangle with two overlapping coords
    b = np.zeros(3)

    try:
        point = tg.triangle(a, a, b)
        assert(False)
    except Exception:
        x = 0

    # a line
    a = np.array((1, 0, 0))
    b = np.array((2, 0, 0))
    c = np.array((3, 0, 0))

    try:
        point = tg.triangle(a, b, c)
        assert(False)
    except Exception:
        x = 0

def test(argv):

    valid_triangles()
    degenerate_triangles()
    point_is_vertex()
    point_in_triangle()
    outside_point_closest_to_vertex()
    outside_point_closest_to_edge()
    barycentric_coords_accurate()

if __name__=="__main__":
    test(sys.argv[1:])
