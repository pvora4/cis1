"""
Parth Vora & Miguel Inserni
CIS I PA5
"""
import numpy as np
import sys

import IO
import transformations as tf
import triangles as tg
import bounding_spheres as bsph
import bounded_octree as boct
import ICP

"""
Function that computes the location of the pointer tip with respect to rigid body B
This is done by registering each frame of the bodies to the tracker to compute FA and FB, then the location of the pointer is simply FB.inv(FA.dot(tip))

args:
    Abodyfile: the file containing rigid body A
    Abodyfile: the file containing rigid body B
    samplefile: the file containing sample readings
return:
    dk: The location of the tip w.r.t. rigid body B
"""
def compute_pointer_tip(Abodyfile, Bbodyfile, samplefile):
    Na, A_LED, A_tip = IO.read_body_file(Abodyfile)
    Nb, B_LED, B_tip = IO.read_body_file(Bbodyfile)
    A_tracker, B_tracker, D_tracker = IO.read_sample_readings(samplefile, Na, Nb)

    # Compute FA,k and FB,k
    FA, FB = [], []
    for k in range(A_tracker.shape[0]):
        FA.append(tf.register(A_LED, A_tracker[k]))
        FB.append(tf.register(B_LED, B_tracker[k]))

    # Compute dk
    dk = []
    for k in range(A_tracker.shape[0]):
        dk.append(FB[k].inv(FA[k].dot(A_tip)))
    
    dk = np.array(dk)
    return dk

"""
Function that constructs triangles, bounding spheres, and bounded octree.

args:
    array of modes
    array of triangle inds
returns:
    bounded octree
"""
def update_structure(model, modes, triangle_inds):

    spheres = []
    for i in range(triangle_inds.shape[0]):
        
        # Triangle vertices
        v1, v2, v3 = triangle_inds[i, 0:3]

        p1 = model[triangle_inds[i, 0]]
        p2 = model[triangle_inds[i, 1]]
        p3 = model[triangle_inds[i, 2]]

        # Triangle modes (vertex, mode, coord)
        tmodes = np.zeros((3, modes.shape[0], 3), dtype=float)
        tmodes[0] = modes[:, v1]
        tmodes[1] = modes[:, v2]
        tmodes[2] = modes[:, v3]

        spheres.append(bsph.sphere(p1, p2, p3, tmodes))

    octree = boct.octree(spheres)
    return octree

"""
Function that updates mesh model given mode weights.
args:
    array of modes
    mode weights
returns:
    updated model
"""
def update_mesh(modes, mode_weights):

    model = np.zeros(modes[0].shape)
    
    for i in range(model.shape[0]):
        model[i] = modes[0, i]
        for j in range(mode_weights.shape[0]):
            model[i] += mode_weights[j] * modes[j+1, i]

    return model

"""
Function that computes mode weights given estimations of sk, ck
args:
    sk = Freg * dk
    ck = closest points on mesh to sk
    spheres = spheres corresponding to ck
    array of modes
returns:
    mode weights
"""
def compute_weights(sk, ck, spheres, modes):

    Nmodes = modes.shape[0] - 1
    Nspheres = len(spheres)

    A, b = [], []

    # Build matrices A and b to solve A(lambda) = b
    for i in range(Nspheres):
        
        # Compute q using barycentric coords of ck
        s = spheres[i]
        u, v, w = s.compute_barycentric_coords(ck[i])
        q = u * s.modes[0] + v * s.modes[1] + w * s.modes[2]
        
        # Add q into A and b
        index = 3*i
        A.append(q[1:, 0])
        A.append(q[1:, 1])
        A.append(q[1:, 2])
        b.append(sk[i, 0] - q[0, 0])
        b.append(sk[i, 1] - q[0, 1])
        b.append(sk[i, 2] - q[0, 2])

    # Solve least squares problem to get weights
    A = np.array(A)
    b = np.array(b)
    Lambda = np.linalg.lstsq(A, b, rcond=None)[0]
    return Lambda

"""
Function that iteratively estimates Freg, mode weights
args:
    dk
    array of modes
    triangle indices
returns:
    Freg
    mode weights
    sk
    ck
"""
def iterate_modes(dk, modes, triangle_inds):

    mode_weights = np.ones(modes.shape[0] - 1, dtype=float)
    converged = False
    stop_threshold = 0.01
    max_epochs = 10

    octree = update_structure(modes[0], modes, triangle_inds)

    epochs = 0
    while not converged:
       
        print("epoch:{0}".format(epochs))
        
        # Use ICP to estimate Freg
        Freg, sk, ck, spheres = ICP.ICP(dk, octree, 20)

        # Estimate mode weights
        prev_weights = mode_weights
        mode_weights = compute_weights(sk, ck, spheres, modes)

        # using mode weights, update model
        model = update_mesh(modes, mode_weights)
        octree = update_structure(model, modes, triangle_inds)

        # convergence if mode weights not changing
        diff = np.sqrt(np.sum(np.square(prev_weights - mode_weights)))
        if diff < stop_threshold or epochs == max_epochs:
            converged = True

        epochs += 1

    # Perform final ICP with a higher number of epochs
    Freg, sk, ck, spheres = ICP.ICP(dk, octree, 50)

    return Freg, mode_weights, sk, ck

def main(argv):

    # Input files for program
    Abodyfile = argv[0]
    Bbodyfile = argv[1]
    meshfile = argv[2]
    samplefile = argv[3]
    modefile = argv[4]
    outfile = argv[5]
    
    #Read debug output
    d_out, c_out, dist_out = read_output(outfile);
    
    # Read in triangle data
    vertices, triangle_inds = IO.read_mesh_file(meshfile)

    # Read in modes
    modes = IO.read_modes(modefile)
    
    
    # Compute dk
    dk = compute_pointer_tip(Abodyfile, Bbodyfile, samplefile)
 
    # Iteratively estimate Freg, mode weights
    Freg, mode_weights, sk, ck = iterate_modes(dk, modes, triangle_inds) 

    # Compute distances
    dist = np.sqrt(np.sum(np.square(sk - ck), axis=1))
    
    #Ck-Cout
    debug_dist = np.sqrt(np.sum(np.square(c_out - ck), axis=1))
    
    # Generate output file
    outputfn = outfile.split("/")[1]
    IO.print_output(outputfn, ck.shape[0], np.round(sk, 2), np.round(ck, 2), np.round(dist, 3), np.round(mode_weights, 3))

if __name__=="__main__":
    main(sys.argv[1:])
