"""
Parth Vora & Miguel Inserni
CIS I PA5
"""
import numpy as np
import transformations as tf
import triangles
import bounding_spheres
import bounded_octree

"""
Function performing iterative closest point algorithm to register pointer tips to the mesh.

args:
    dk: array of pointer tip locations
    octree of bounded spheres
returns:
    Freg transformation
    sk points
    ck points
"""
def ICP(dk, octree, max_epochs):

    # Initialize Freg to identity
    Freg = tf.transform(np.identity(3), np.zeros(3))

    # ICP parameters
    converged = False
    dist_threshold = 1
    stop_threshold = 0.001 # Average error is < 0.001mm

    epochs = 0
    while not converged:

        w = np.zeros(dk.shape[0])
        sk = np.zeros(dk.shape, dtype=float)
        ck = np.zeros(dk.shape, dtype=float)
        spheres = []
        for i in range(dk.shape[0]):

            # Apply Freg to compute sample points
            sk[i] = Freg.dot(dk[i])

            # Compute closest points on mesh to sample points
            ck[i], bound, sphere = octree.search(octree.root, sk[i])
            spheres.append(sphere)

            # If dist is within a threshold, we keep it.
            dist = np.linalg.norm(ck[i] - sk[i])
            if dist < dist_threshold:
                w[i] = 1

        # Get inlier points
        dk_inliers = dk[np.where(w == 1)]
        ck_inliers = ck[np.where(w == 1)]

        # Reestimate Freg on inliers by registering dk to ck
        Fprev = Freg
        Freg = tf.register(dk_inliers, ck_inliers)

        # Evaluate converged condition
        diff = np.sqrt(np.sum(np.square(sk - ck))) / sk.shape[0]
        #print(diff)
        if diff < stop_threshold or epochs == max_epochs:
            converged = True

        epochs += 1

    return Freg, sk, ck, spheres

