"""
Parth Vora & Miguel Inserni
CIS I PA4
"""
import sys
import numpy as np
import triangles as tg
import bounding_spheres as bsph

"""
Octree of bounded spheres. Each node of the octree is also a bounded sphere.
Input: list of bounded sphere objects
Output: octree of spheres
"""
class octree:
    
    """
    Nested Octree node class
    Input: list of bounded sphere objects
    Output: none
    """
    class octree_node:

        """
        Initialize octree node
        """
        def __init__(self, spheres):
            
            self.spheres = spheres
            self.subtrees = None
            self.valid = True
            
            self.center = None
            self.radius = None    
        
            self.has_subtrees = False

            if len(spheres) == 0:
                self.valid = False
            
            # Base case: < 10 nodes not worth splitting
            elif len(spheres) < 10:
                self.center, self.radius = self.compute_center(spheres)

            # >32 spheres = Divide the space
            else:
                self.has_subtrees = True
                self.center, self.radius = self.compute_center(spheres)

        """
        Compute centroid of spheres
        Input: list of sphere objects
        Output: centroid and maximum radius
        """
        def compute_center(self, spheres):

            center = np.zeros(3)
            for s in spheres:
                center += s.q
                
            center /= len(spheres)

            radius = 0
            for s in spheres:
                dist = np.linalg.norm(center - s.q) + s.r
                if dist > radius:
                    radius = dist

            return center, radius

    """
    Initialize Octree
    Calls build_tree to construct the tree
    """
    def __init__(self, spheres):
        self.root = self.build_tree(spheres)

    """
    Function that recursively builds the octree structure
    Input: list of spheres
    Output: octree_node
    """
    def build_tree(self, spheres):
        
        # Build a node
        node = self.octree_node(spheres)

        # If node is empty, unitary, or only has same points, return no subtrees
        if not node.has_subtrees:
            return node

        # Else, compute subtrees
        # Subtrees are a dictionary of {octant -> node}
        split_spheres = {}
        for i in range(0, 2):
            for j in range(0, 2):
                for k in range(0, 2):
                    split_spheres[(i, j, k)] = []

        # Split spheres into octants: 0 if less than, 1 if greater than
        for s in spheres:
            i = int(s.q[0] > node.center[0])   
            j = int(s.q[1] > node.center[1])   
            k = int(s.q[2] > node.center[2])
            split_spheres[(i, j, k)].append(s)
        
        # Each subtree is another octree node
        subtrees = {}
        for key in split_spheres.keys():
            subtrees[key] = self.build_tree(split_spheres[key])
            
        node.subtrees = subtrees
        return node

    def linsearch(self, spheres, point):
        closest_point = None
        bound = np.inf
        closest_sphere = None
        for s in spheres:
                circbound = np.linalg.norm(s.q - point) - s.r
                if circbound <= bound:

                    p = s.closest_point(point)

                    inbound = np.linalg.norm(p - point)
                    if inbound < bound:
                        closest_point = p
                        closest_sphere = s
                        bound = inbound
            
        return closest_point, bound, closest_sphere

    """
    Function that searches octree to find closest point on mesh
    Input: point (np array 3x3)
    Output: closest point on mesh(np array 3x3)
    """
    def search(self, node, point):
    
        if not node.has_subtrees:
            return self.linsearch(node.spheres, point)

        maxbound = np.inf
        best_point = np.zeros(3)
        best_sphere = None
        for k in node.subtrees.keys():

            nextnode = node.subtrees[k]
            if len(nextnode.spheres) == 0:
                continue

            if np.linalg.norm(point - nextnode.center) <= nextnode.radius:
                
                cp, bound, sphere = self.search(nextnode, point)

                if bound < maxbound:
                    maxbound = bound
                    best_point = cp
                    best_sphere = sphere
        
        if best_sphere == None:
            return self.linsearch(node.spheres, point)

        return best_point, maxbound, best_sphere
    
