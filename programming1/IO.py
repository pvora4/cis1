import sys
import numpy as np

def read_calibration(fn):
    f = open(fn)

    # Get calibration description
    description = f.readline().split(",")
    D_size = int(description[0])
    A_size = int(description[1])
    C_size = int(description[2])

    D_cal = np.zeros((D_size, 3), dtype=float)
    A_cal = np.zeros((A_size, 3), dtype=float)
    C_cal = np.zeros((C_size, 3), dtype=float)
        
    for i in range(D_size):
        line = f.readline().split(",")
        D_cal[i] = np.asarray(line, dtype=float)

    for i in range(A_size):
        line = f.readline().split(",")
        A_cal[i] = np.asarray(line, dtype=float)

    for i in range(C_size):
        line = f.readline().split(",")
        C_cal[i] = np.asarray(line, dtype=float)

    f.close()
    return D_cal, A_cal, C_cal

def read_frames(fn):
    f = open(fn)

    # Get file description
    description = f.readline().split(",")
    description.pop(-1) # get rid of last string

    # Get number of frames at end
    n_frames = int(description[-1])
    description.pop(-1)
    
    # Get elements with n_elems each until list is empty
    elem_lengths = []
    for i in range(0, len(description)):
        elem_lengths.append(int(description[i]))

    elems = []
    for i in range(0, len(elem_lengths)):
        elems.append(np.zeros((n_frames, elem_lengths[i], 3), dtype=float))

    # For every frame
    for i in range(0, n_frames):

        # For every element
        for j in range(0, len(elem_lengths)):

            # For the length of the jth element, read that many lines
            for k in range(0, elem_lengths[j]):
                line = f.readline().split(",")
                elems[j][i][k] = np.asarray(line, dtype=float)

    f.close()
    return elems

def read_output(fn):
    f = open(fn)

    # Get file description
    description = f.readline().split(",")
    
    # location of EM and OPT pointers
    p_EM = np.asarray(f.readline().split(","), dtype=float)
    p_opt = np.asarray(f.readline().split(","), dtype=float)

    Ci = []
    for line in f:
        Ci.append(np.asarray(line.split(","), dtype=float))

    return p_EM, p_opt, Ci





