#!/bin/bash

calbody=(
  "Data/pa1-unknown-h-calbody.txt"
  "Data/pa1-unknown-i-calbody.txt"
  "Data/pa1-unknown-j-calbody.txt"
  "Data/pa1-unknown-k-calbody.txt")

calreadings=(
  "Data/pa1-unknown-h-calreadings.txt"
  "Data/pa1-unknown-i-calreadings.txt"
  "Data/pa1-unknown-j-calreadings.txt"
  "Data/pa1-unknown-k-calreadings.txt")

empivot=(
  "Data/pa1-unknown-h-empivot.txt"
  "Data/pa1-unknown-i-empivot.txt"
  "Data/pa1-unknown-j-empivot.txt"
  "Data/pa1-unknown-k-empivot.txt")

optpivot=(
  "Data/pa1-unknown-h-optpivot.txt"
  "Data/pa1-unknown-i-optpivot.txt"
  "Data/pa1-unknown-j-optpivot.txt"
  "Data/pa1-unknown-k-optpivot.txt")

function dc {
  echo "RUNNING: Distortion Calibration"
  mode="dc"
  for i in {0..3}; do
    python3 main.py $mode ${calreadings[$i]} ${calbody[$i]}
  done
}

function epc {
  echo "RUNNING: EM Pivot Calibration"
  mode="epc"
  for i in {0..3}; do
    python3 main.py $mode ${empivot[$i]}
  done
}

function opc {
  echo "RUNNING: Optical Pivot Calibration"
  mode="opc"
  for i in {0..3}; do
    python3 main.py $mode ${optpivot[$i]} ${calbody[$i]}
  done
}

dc
epc
opc
