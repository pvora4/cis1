import numpy as np
import sys

import IO
import functions
from functions import transform

"""
Function that returns sum of squares distance between two vectors
"""
def compute_error(w, v):
    return (w[0] - v[0]) ** 2 + (w[1] - v[1]) ** 2 + (w[2] - v[2]) ** 2

"""
Question 4: 
    Calculates Ci for every frame
"""
def distortion_calibration(frame_file, cal_file):
    Dcal, Acal, Ccal = IO.read_calibration(cal_file)
    elements = IO.read_frames(frame_file)
    D, A, C = elements[0], elements[1], elements[2]

    n_frames = D.shape[0]
    Cexp = []
    for i in range(n_frames):
        Fd = functions.register(Dcal, D[i])
        Fa = functions.register(Acal, A[i])
        
        for j in range(len(Ccal)):
            Ci = Fd.inv(Fa.dot(Ccal[j]))
            Cexp.append(Ci)

    return Cexp

"""
Question 5:
    Performs Pivot calibration on EM tracking data
"""
def EM_pivot_calibration(frame_file):
    elements = IO.read_frames(frame_file)
    elem = elements[0]
    tg, pd = functions.pivot_calibrate(elem)
    return pd


"""
Question 6:
    Performs pivot calibration on optical tracking data
"""
def optical_pivot_calibration(frame_file, cal_file):
    Dcal, Acal, Ccal = IO.read_calibration(cal_file)
    elements = IO.read_frames(frame_file)
    D = elements[0]
    H = elements[1]
    n_frames = D.shape[0]

    # Convert H to EM coordinates
    H_EM = np.zeros(H.shape, dtype=float)
    for i in range(n_frames):
        Fd = functions.register(Dcal, D[i])
        for j in range(H[i].shape[0]):
            H_EM[i][j] = Fd.inv(H[i][j])

    tg, pd = functions.pivot_calibrate(H_EM)
    return pd

"""
Main arguments:
    1. mode (dc-test, epc-test, opc-test, dc, epc, opc)
    2. (Only for test) output file for verification
    3. frames file
    4. (Only for dc and opc) calibration file
"""
def main(argv):
    
    mode = argv[0]
    
    if mode == "dc-test":
        Ci = distortion_calibration(argv[2], argv[3])
        output = argv[1]
        p_EM, p_opt, Ci_out = IO.read_output(output)

        error = 0
        for i in range(len(Ci_out)):
            error += compute_error(Ci_out[i], Ci[i])
        
        avg_error = error / len(Ci_out)
        print(Ci[0])
        print(Ci_out[0])
        print("Ci average SSD error {:.2f}".format(avg_error))

    elif mode == "epc-test":
        pd = EM_pivot_calibration(argv[2])
        output = argv[1]
        p_EM, p_opt, Ci_out = IO.read_output(output)

        error = compute_error(p_EM, pd)
        print(pd)
        print(p_EM)
        print("EM SSD error {:.2f}".format(error))

    elif mode == "opc-test": 
        pd = optical_pivot_calibration(argv[2], argv[3])
        output = argv[1]
        p_EM, p_opt, Ci_out = IO.read_output(output)

        error = compute_error(p_opt, pd)
        print(pd)
        print(p_opt)
        print("Optical SSD error {:.2f}".format(error))

    elif mode == "dc":
        Ci = distortion_calibration(argv[1], argv[2])
        for i in Ci:
            print(i)

    elif mode == "epc":
        pd = EM_pivot_calibration(argv[1])
        print(pd)

    elif mode == "opc":
        pd = optical_pivot_calibration(argv[1], argv[2])
        print(pd)

    else:
        print("Error: invalid mode")

if __name__ == '__main__':
    main(sys.argv[1:])
