#!/bin/bash

# Calbody files
calbody=("Data/pa1-debug-a-calbody.txt" 
  "Data/pa1-debug-b-calbody.txt"
  "Data/pa1-debug-c-calbody.txt"
  "Data/pa1-debug-d-calbody.txt"
  "Data/pa1-debug-e-calbody.txt"
  "Data/pa1-debug-f-calbody.txt"
  "Data/pa1-debug-g-calbody.txt")

calreadings=(
  "Data/pa1-debug-a-calreadings.txt"
  "Data/pa1-debug-b-calreadings.txt"
  "Data/pa1-debug-c-calreadings.txt"
  "Data/pa1-debug-d-calreadings.txt"
  "Data/pa1-debug-e-calreadings.txt"
  "Data/pa1-debug-f-calreadings.txt"
  "Data/pa1-debug-g-calreadings.txt")

empivot=(
  "Data/pa1-debug-a-empivot.txt"
  "Data/pa1-debug-b-empivot.txt"
  "Data/pa1-debug-c-empivot.txt"
  "Data/pa1-debug-d-empivot.txt"
  "Data/pa1-debug-e-empivot.txt"
  "Data/pa1-debug-f-empivot.txt"
  "Data/pa1-debug-g-empivot.txt")

optpivot=(
  "Data/pa1-debug-a-optpivot.txt"
  "Data/pa1-debug-b-optpivot.txt"
  "Data/pa1-debug-c-optpivot.txt"
  "Data/pa1-debug-d-optpivot.txt"
  "Data/pa1-debug-e-optpivot.txt"
  "Data/pa1-debug-f-optpivot.txt"
  "Data/pa1-debug-g-optpivot.txt")

output=(
  "Data/pa1-debug-a-output1.txt"
  "Data/pa1-debug-b-output1.txt"
  "Data/pa1-debug-c-output1.txt"
  "Data/pa1-debug-d-output1.txt"
  "Data/pa1-debug-e-output1.txt"
  "Data/pa1-debug-f-output1.txt"
  "Data/pa1-debug-g-output1.txt")

# Test distortion calibration
function test_dc {
  echo "TESTING: Distortion Calibration"
  mode="dc-test"
  for i in {0..6}; do
    python3 main.py $mode ${output[$i]} ${calreadings[$i]} ${calbody[$i]}

  done

}

function test_epc {
  echo "TESTING: EM Pivot Calibration"
  mode="epc-test"
  for i in {0..6}; do
    python3 main.py $mode ${output[$i]} ${empivot[$i]} ${calbody[$i]}

  done
}

function test_opc {
  echo "TESTING: Optical Pivot Calibration"
  mode="opc-test"
  for i in {0..6}; do
    python3 main.py $mode ${output[$i]} ${optpivot[$i]} ${calbody[$i]}

  done
}

test_dc
test_epc
test_opc
