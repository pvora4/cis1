#!/bin/bash

# Enter parameters here
MODE="dc-test"

# If running on test
OUTPUT="Data/pa1-debug-a-output1.txt"

FRAMES="Data/pa1-debug-a-calreadings.txt"

CALIBRATION="Data/pa1-debug-a-calbody.txt"

python3 main.py $MODE $OUTPUT $FRAMES $CALIBRATION
