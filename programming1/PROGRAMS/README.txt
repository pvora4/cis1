Files included:

main.py:
  main file running distortion calibration, EM pivot calibration, optical pivot calibratino.
  Args:
    1. mode = dc, epc, opc, dc-test, epc-test, opc-test
    if mode = dc:
      2. file with calibration frame data
      3. file with calibration object
    if mode = epc:
      2. file with EM frame data
    if mode = opc:
      2. file with optical frame data
      3. file with calibration object
    if mode = dc-test:
      2. file with output verification file
      3. file with calibration frame data
      4. file with calibration object
    if mode = epc-test:
      2. file with output verification file
      3. file with EM frame data
    if mode = opc-test:
      2. file with output verification file
      3. file with optical frame data
      4. file with calibration object

  Output (printed to stdout):
    if mode = dc:
      Calculated Cis for every frame
    if mode = epc or opc:
      pointer location
    if running in test mode:
      above output values
      values from output.txt file
      SSD error between calculated and debug values

functions.py:
  Contains registration and pivot calibration algorithms

IO.py:
  Performs file IO

unit_test.py:
  Contains unit tests

Shell scripts:

run.sh:
  Script to run the program. Change file paths as necessary, then run with ./run.sh

debug_tests.sh:
  Script that runs all modes in test mode.

unknown_results.sh:
  Script that generates results for all unknown files for all modes.

OUTPUT files
debug.txt:
  Results from running debug_tests.sh. All modes run in test mode.

unknown.txt:
  Results from running unknown_results.sh. All modes run normally.

