import sys
import numpy as np

"""
A class that holds Frame transformations

dot:
    args: vector
    returns: Frame transformed vector

inv:
    args: vector
    returns: Inverse of frame transformation on vector
"""

class transform:
    def __init__(self, _R, _p):
        self.R = _R
        self.Rinv = np.linalg.inv(_R)
        self.p = _p

    def dot(self, v):
        return self.R @ v + self.p

    def inv(self, v):
        return self.Rinv @ v - self.Rinv @ self.p

"""
A function to calculate registration transformations using SVD

Input:
    A: (d x 3) point cloud
    B: (d x 3) point cloud
Return: Transform object, Freg = [R, p]
    R: (3 x 3) rotation matrix
    p: (1 x 3) position vector
"""
def register(A, B):

    # Step 1: make A, B zero mean to align centroids 
    A_bar = A - np.mean(A, axis=0)
    B_bar = B - np.mean(B, axis=0)

    # Step 2: Find optimal rotation with SVD
    H = B_bar.T @ A_bar
    U, S, Vt = np.linalg.svd(H)
    R = U @ Vt

    # Step 3: Handling Edge case (rare)
    # Ensure that determinant is not -1
    if np.linalg.det(R) == -1:
        
        # Find where singular value is small due to FP errors
        i = np.argwhere(S < 1)
        
        # Algorithm breaks if there is no small singular value
        # However, this is exceedingly rare.
        if not i:
            print(S)
            sys.stderr.write("Error: Registration cannot be handled by SVD. \n")
            assert False
        
        # Flip the sign of the column where singular value is small, construct new R
        Vprime = Vt.T
        Vprime[:,i] *= -1
        R = U @ Vprime.T

    # Step 4: Find p
    p = np.mean(B, axis=0) - R @ np.mean(A, axis=0)
    
    return transform(R, p)

"""
A function performing pivot calibration

Input:
    Frames: n_frames x points x 3

Return:
    p_tip (3 x 1), p_pivot (3 x 1)

"""
def pivot_calibrate(frames):

    n_frames, n_trackers = frames.shape[0], frames.shape[1]
    # list of rotations and positions
    Rots, ps = [], []

    # Center of pivot
    g = frames[0] - np.mean(frames[0], axis=0)

    for i in range(0, n_frames): 
    
        G = frames[i]

        # Register g to G
        Fg = register(g, G)
        Rots.append(Fg.R)
        ps.append(Fg.p)

    # Concatenate matrices
    # [Rgk | -I], [-Pgk] 
    identity = np.identity(3, dtype=float) * -1
    Rtot = np.concatenate((Rots[0], identity), axis=1)
    ptot = ps[0]*-1
    
    for i in range(1, n_frames):
        Rcell = np.concatenate((Rots[i], identity), axis=1)
        Rtot = np.concatenate((Rtot, Rcell), axis=0)
        ptot = np.concatenate((ptot, ps[i]*-1), axis=0)

    # Solve least squares
    # A = Rtot, B = ptot
    # dim of return should be 1x6 = [tg | pd]
    x = np.linalg.lstsq(Rtot, ptot, rcond=None)[0]
   
    tg = x[0:3]
    pd = x[3:6]
    return tg, pd
