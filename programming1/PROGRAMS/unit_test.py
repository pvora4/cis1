import sys
import numpy as np
import functions
from functions import transform

# Ensure that the shape of the rotation matrix is 3x3
def registration_shape():
    A = np.random.rand(10, 3) * 1000
    F = functions.register(A, A)
    assert(F.R.shape[0] == F.R.shape[1] and F.R.shape[0] == 3)

# Ensure that the rotation matrix for two same frames is identity
def identity_transform():
    A = np.random.rand(10, 3) * 1000
    F = functions.register(A, A)
    R = F.R

    Id = np.identity(3, dtype=int)
    for i in range(R.shape[0]):
        for j in range(R.shape[1]):
            assert(int(np.round(R[i, j], 2)) == Id[i, j])

# Ensure that F-1(F(a)) = F(F-1(a)) = a
def frame_transforms():
    A = np.random.rand(10, 3) * 1000
    B = np.random.rand(10, 3) * 1000
    Fb = functions.register(A, B)
   
    assert(Fb.inv(Fb.dot(A[0])).all() == A[0].all())
    assert(Fb.dot(Fb.inv(B[0])).all() == B[0].all())

def main():

    for i in range(10):
        registration_shape()
        identity_transform()
        frame_transforms()

if __name__ == '__main__':
    main()
