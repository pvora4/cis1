Files included:

PROGRAM files:

distortion_calibration.py:
  File that applies distortion correction to EM data, registers EM coordinates to CT coordinates, and outputs tip coordinates in CT coordinates.

  ARGS:
  1. output_file
  2. calread.txt
  3. calbody.txt
  4. em_pivot.txt
  5. em-fiducialss.txt
  6. ct-fiducials.txt
  7. EM-nav.txt
  8. optpivot.txt

  You can run this program by editing run.sh to match your files, and then executing with ./run.sh. Alternatively, run with python3 ${ARGS}
  
functions.py:
  File with various useful functions. This file includes algorithms and classes from PA1, including registration, pivot calibration, and frame transformation. To this file, a distortion model class was added, which uses bernstein polynomials to model warping. 

IO.py:
  Performs file IO.

unit_tests.py:
  Contains unit tests. Executed with ./unit_tests.py

run.sh:
  Runs distortion_calibration.py as state above.

OUTPUT files:
  pa2-unknown-{g, h, i, j}-output-{1, 2}.txt

