import sys
import numpy as np
import scipy as sp

"""
A class that holds Frame transformations

dot:
    args: vector
    returns: Frame transformed vector

inv:
    args: vector
    returns: Inverse of frame transformation on vector
"""

class transform:
    def __init__(self, _R, _p):
        self.R = _R
        self.Rinv = np.linalg.inv(_R)
        self.p = _p

    def dot(self, v):
        return self.R @ v + self.p

    def inv(self, v):
        return self.Rinv @ v - self.Rinv @ self.p
"""
A class that holds a distortion model

scale_to_box:
    args: frame data
    returns: normalized data to unit box

bernstein_expansion:
    args: vector
    returns: the 5th order bernstein expansion of the vector

train:
    args: input frames, output frames
    returns: bernstein coefficients, mins, maxs of input data

run:
    args: frame
    returns: warped frame

"""
class distortion_model:
    def __init__(self):
        self.coeffs = None
        self.mins = None
        self.maxs = None
        self.isTrained = False

    def scale_to_box(self, frame):
        if not self.isTrained:
            self.mins, self.maxs = np.zeros(3), np.zeros(3)
            self.mins[0] = np.min(frame[:, 0])
            self.mins[1] = np.min(frame[:, 1])
            self.mins[2] = np.min(frame[:, 2])
    
            self.maxs[0] = np.max(frame[:, 0])
            self.maxs[1] = np.max(frame[:, 1])
            self.maxs[2] = np.max(frame[:, 2])
            self.isTrained = True

        frame_scaled = np.copy(frame)
        for i in range(0, 3):
            frame_scaled[:, i] -= self.mins[i]
            frame_scaled[:, i] /= (self.maxs[i] - self.mins[i])

        return frame_scaled

    def bernstein_expansion(self, v):
         # (5 i) coefficients for all i
        five_choose_ = []
        for i in range(0, 6):
            five_choose_.append(sp.special.binom(5, i))

        # Generate terms of bernstein polynomials from 0th order to 5th order
        x, y, z = [], [], []
        for i in range(0, 6):
            x.append(five_choose_[i] * (v[0] ** i) * ((1 - v[0]) ** (5 - i)))
            y.append(five_choose_[i] * (v[1] ** i) * ((1 - v[1]) ** (5 - i)))
            z.append(five_choose_[i] * (v[2] ** i) * ((1 - v[2]) ** (5 - i)))

        # Multiply all permutations of terms
        combinations = np.zeros(216)
        for i in range(0, 6):
            for j in range(0, 6):
                for k in range(0, 6):
                    index = 36*i + 6*j + k
                    combinations[index] = x[i] * y[j] * z[k]
        return combinations

    def train(self, inputs, outputs):
        # Reshape the inputs and outputs to 2D
        inframes = inputs.reshape(
                inputs.shape[0] * inputs.shape[1], inputs.shape[2])
        outframes = outputs.reshape(
                outputs.shape[0] * outputs.shape[1], outputs.shape[2])
        
        # Scale input to box
        infr_scaled = self.scale_to_box(inframes)
        #print(infr_scaled)
        # Get bernstein terms
        b_terms = np.zeros((inframes.shape[0], 216))
        for i in range(0, inframes.shape[0]):
            b_terms[i] = self.bernstein_expansion(infr_scaled[i])
            
        self.coeffs = np.zeros((3, 216), dtype = float)
        for i in range(0, 3):           
            self.coeffs[i] = np.linalg.lstsq(b_terms, outframes[:,i], rcond=None)[0]

    def run(self, frame):
        fr_scaled = self.scale_to_box(frame)
        warped_frame = np.zeros(fr_scaled.shape)
        for i in range(fr_scaled.shape[0]):
            bterm = self.bernstein_expansion(fr_scaled[i])
            warped_frame[i] = bterm @ self.coeffs.T
        return warped_frame

"""
A function to calculate registration transformations using SVD

Input:
    A: (d x 3) point cloud
    B: (d x 3) point cloud
Return: Transform object, Freg = [R, p]
    R: (3 x 3) rotation matrix
    p: (1 x 3) position vector
"""
def register(A, B):

    # Step 1: make A, B zero mean to align centroids 
    A_bar = A - np.mean(A, axis=0)
    B_bar = B - np.mean(B, axis=0)

    # Step 2: Find optimal rotation with SVD
    H = B_bar.T @ A_bar
    U, S, Vt = np.linalg.svd(H)
    R = U @ Vt

    # Step 3: Handling Edge case (rare)
    # Ensure that determinant is not -1
    if np.linalg.det(R) == -1:
        
        # Find where singular value is small due to FP errors
        # Algorithm breaks if there is no small singular value
        # However, this is exceedingly rare.
       
        i = np.argwhere(S < 1) 
        if not i:
            print(S)
            sys.stderr.write("Error: Registration cannot be handled by SVD. \n")
            assert False
        
        # Flip the sign of the column where singular value is small, construct new R
        Vprime = Vt.T
        Vprime[:,i] *= -1
        R = U @ Vprime.T

    # Step 4: Find p
    p = np.mean(B, axis=0) - R @ np.mean(A, axis=0)
    
    return transform(R, p)

"""
A function performing pivot calibration

Input:
    Frames: n_frames x points x 3

Return:
    p_tip (3 x 1), p_pivot (3 x 1)

"""
def pivot_calibrate(frames):

    n_frames = frames.shape[0]
    # list of rotations and positions
    Rots, ps = [], []

    # Center of pivot
    g = frames[0] - np.mean(frames[0], axis=0)

    for i in range(0, n_frames): 
    
        G = frames[i]

        # Register g to G
        Fg = register(g, G)
        Rots.append(Fg.R)
        ps.append(Fg.p)

    # Concatenate matrices
    # [Rgk | -I], [-Pgk] 
    identity = np.identity(3, dtype=float) * -1
    Rtot = np.concatenate((Rots[0], identity), axis=1)
    ptot = ps[0]*-1
    
    for i in range(1, n_frames):
        Rcell = np.concatenate((Rots[i], identity), axis=1)
        Rtot = np.concatenate((Rtot, Rcell), axis=0)
        ptot = np.concatenate((ptot, ps[i]*-1), axis=0)

    # Solve least squares
    # A = Rtot, B = ptot
    # dim of return should be 1x6 = [tg | pd]
    x = np.linalg.lstsq(Rtot, ptot, rcond=None)[0]
   
    tg = x[0:3]
    pd = x[3:6]
    return tg, pd
