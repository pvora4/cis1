#!/bin/python3
import sys
import numpy as np
import IO
import functions
from functions import transform
from functions import distortion_model

import distortion_calibration as dc

def validate_scaling():

    for i in range(100):
        rand_size = np.random.randint(1000)
        rand_m = np.zeros((rand_size, 3))
        for j in range(0, 3):
            rand_m[:, j] = np.random.rand(rand_size) * 1000
        
        dcm = functions.distortion_model()
        rand_m_scaled = dcm.scale_to_box(rand_m)
        mins, maxs = dcm.mins, dcm.maxs

        # Assert scaling results in a box
        assert(np.min(rand_m_scaled) == 0)
        assert(np.max(rand_m_scaled) == 1)

        # Assert min values are true min values
        assert(np.min(rand_m[:,0]) == mins[0])
        assert(np.min(rand_m[:,1]) == mins[1])
        assert(np.min(rand_m[:,2]) == mins[2])

        # Assert max values are true max values
        assert(np.max(rand_m[:,0]) == maxs[0])
        assert(np.max(rand_m[:,1]) == maxs[1])
        assert(np.max(rand_m[:,2]) == maxs[2])

# modelling noise between identical sets should
# result in basically an identity transformation
def validate_distortion_model_on_identity():

    for i in range(10):
        randsize = np.random.randint(100)
        randm = np.zeros((randsize, randsize, 3))
        for j in range(0, 3):
            randm[:,:,j] = np.random.rand(randsize, randsize) * 1000

        dcm = functions.distortion_model()
        dcm.train(randm, randm)

        for i in range(randsize):
            v = dcm.run(randm[i])
            assert(np.round(v, 3).all() == np.round(randm[i], 3).all())

    return 0

# pa2-debug-a has no distortion,
# so using model should yield identical results as without
def validate_distortion_model_on_no_distortion():

    calread = "Data/pa2-debug-a-calreadings.txt"
    calbody = "Data/pa2-debug-a-calbody.txt"

    C, C_exp = dc.expected_ci(calread, calbody)
    dcm = functions.distortion_model()
    dcm.train(C, C_exp)

    for i in range(10):
        randsize = np.random.randint(100)
        randm = np.random.rand(randsize, randsize, 3) * 1000

        for i in range(randsize):
            v = dcm.run(randm[i])
            assert(np.round(v, 3).all() == np.round(randm[i], 3).all())

    return 0

def main(argv):

    print("validating scaling... ")
    validate_scaling()

    print("validating dcm on identity... ")
    validate_distortion_model_on_identity()

    print("validating dcm on data with no distortion... ")
    validate_distortion_model_on_no_distortion() 

if __name__ == '__main__':
    main(sys.argv[1:])
