#!/bin/bash

PREFIX="pa2-unknown-j"
OUTPUT="$PREFIX-output2.txt"
CALOBJ="Data/$PREFIX-calbody.txt"
FRAMES="Data/$PREFIX-calreadings.txt"
EMFILE="Data/$PREFIX-empivot.txt"
CTFIDS="Data/$PREFIX-em-fiducialss.txt"
EMFIDS="Data/$PREFIX-ct-fiducials.txt"
EMNAVP="Data/$PREFIX-EM-nav.txt"
OPTPVT="Data/$PREFIX-optpivot.txt"

python3 distortion_calibration.py $OUTPUT $FRAMES $CALOBJ $EMFILE $CTFIDS $EMFIDS $EMNAVP $OPTPVT 

