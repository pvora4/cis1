import numpy as np
import scipy as sp
from scipy import special
import sys

import IO
import functions


np.set_printoptions(threshold=sys.maxsize)

"""
Question 1:
    Calculates expected Ci for every frame
"""
def expected_ci(calread_file, calbody_file):
    Dcal, Acal, Ccal = IO.read_calibration(calbody_file)
    elements = IO.read_frames(calread_file)
    D, A, C = elements[0], elements[1], elements[2]

    n_frames = D.shape[0]
    Ci_exp = np.zeros(C.shape)
    for i in range(n_frames):
        Fd = functions.register(Dcal, D[i])
        Fa = functions.register(Acal, A[i])

        for j in range(len(Ccal)):
            Ci = Fd.inv(Fa.dot(Ccal[j]))
            Ci_exp[i, j] = Ci

    C = np.array(C)
    Ci_exp = np.array(Ci_exp)

    return C, Ci_exp

"""
Question 3:
    EM pivot calibration with distortion function
"""
def EM_pivot_calibration_with_distortion(frame_file, dcm):
    elements = IO.read_frames(frame_file)
    EM_frames = elements[0]
   
    # Apply Distortion function to every frame
    EM_frames_dc = np.zeros(EM_frames.shape)
    for f in range(EM_frames.shape[0]):
        EM_frames_dc[f] = dcm.run(EM_frames[f])

    # Compute new pivot point
    tg, pd = functions.pivot_calibrate(EM_frames_dc)
    return tg, EM_frames_dc[0]

""" 
Question 4:
    Recompute (Cx_Nc, Cy_Nc, Cz_Nc) with distortion function
"""
def recompute_Cis_with_distortion(Ci, dcm, em_file, opt_file, cal_body):

    # Cis with warping
    Ci_dc = np.zeros(Ci.shape)
    for f in range(Ci.shape[0]):
        Ci_dc[f] = dcm.run(Ci[f])

    # Em calibration
    G = IO.read_frames(em_file)[0]
    G_warp = np.zeros(G.shape)
    for i in range(G.shape[0]):
        G_warp[i] = dcm.run(G[i])

    em_tg, em_pd = functions.pivot_calibrate(G_warp)

    # optical calibration
    Dcal, Acal, Ccal = IO.read_calibration(cal_body)
    elements = IO.read_frames(opt_file)
    D, H = elements[0], elements[1]
    n_frames = D.shape[0]

    H_EM = np.zeros(H.shape)
    for i in range(n_frames):
        Fd = functions.register(Dcal, D[i])
        for j in range(H[i].shape[0]):
            H_EM[i, j] = Fd.inv(H[i, j])

    opt_tg, opt_pd = functions.pivot_calibrate(H_EM)

    return Ci_dc, em_pd, opt_pd

"""
Question 5:
    Compute Freg from CT to EM
"""
def compute_Freg(em_fid_file, ct_fid_file, dcm, ptip, em_cloud):
    ct_fids = IO.read_ctfiducials(ct_fid_file)
    ct_fids = np.array(ct_fids)
    em_fids = IO.read_frames(em_fid_file)[0]
    
    # Get EM position of tip
    em_tips = np.zeros((em_fids.shape[0], 3))
    for i in range(em_fids.shape[0]):
        warp_frame = dcm.run(em_fids[i])
        F_frame = functions.register(em_cloud, warp_frame)
        em_tips[i] = F_frame.dot(ptip + np.mean(em_cloud, axis=0))

    # Compute Freg
    Freg = functions.register(em_tips, ct_fids)
    return Freg

"""
Question 6:
    
"""

def EM_to_CT(em_nav_file, Freg, dcm, ptip, em_cloud):
    em_nav = IO.read_frames(em_nav_file)[0]
    em_nav = np.array(em_nav)
    ct_tips = np.zeros((em_nav.shape[0], 3))

    for i in range(em_nav.shape[0]):
        warp_frame = dcm.run(em_nav[i])
        F_frame = functions.register(em_cloud, warp_frame)
        tip_em = F_frame.dot(ptip + np.mean(em_cloud, axis=0))
        ct_tips[i] = Freg.dot(tip_em)
       
    return ct_tips

def compute_error(w, v):
    return (w[0] - v[0]) ** 2 + (w[1] - v[1]) ** 2 + (w[2] - v[2]) ** 2

def main(argv):

    # File Paths
    output_file = argv[0]
    calread_file = argv[1]
    calbody_file = argv[2]
    EM_file = argv[3]
    ct_fid_file = argv[4]
    em_fid_file = argv[5]
    em_nav_file = argv[6]
    opt_file = argv[7]
    
    # Read Cis, and calculated Ci_exps
    Ci, Ci_exp = expected_ci(calread_file, calbody_file)
   
    # Train the distortion model
    print("Training Distortion model")
    dcm = functions.distortion_model()
    dcm.train(Ci, Ci_exp)

    # Perform pivot calibration with distortion model
    print("Calculating Warped EM pivot position")
    warp_pd, EM_point_cloud = EM_pivot_calibration_with_distortion(
            EM_file, dcm)

    # Calculated warped Ci points
    print("Calculating Warped Cis")
    warp_Ci, em_pd, opt_pd = recompute_Cis_with_distortion(Ci, dcm, EM_file, opt_file, calbody_file) 
    
    """ For printing output.txt and computing error
    print(str(warp_Ci.shape[0]) + "," +  output_file)
    warp_Ci = warp_Ci.reshape((warp_Ci.shape[0]*warp_Ci.shape[1],3))
    
    print(warp_Ci)
    ssd = 0

    for i in range(warp_Ci.shape[0]):
        ssd += compute_error(Ci_out[i], warp_Ci[i])
                            
    ssd = ssd / warp_Ci.shape[0]
    print(ssd)
    """

    # Calculate registration frame Freg
    print("Computing Freg")
    Freg = compute_Freg(ct_fid_file, em_fid_file, dcm, warp_pd, EM_point_cloud)

    # Convert EM nav data to CT
    print("Converting EM tip to CT tip")
    ct_tips = EM_to_CT(em_nav_file, Freg, dcm, warp_pd, EM_point_cloud) 
    print(ct_tips)
    
if __name__ == '__main__':
    main(sys.argv[1:])

