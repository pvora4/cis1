"""
Parth Vora & Miguel Inserni
CIS I PA3
11/21/2019
"""

import numpy as np
import sys

import IO
import transformations as tf
import triangles as tg

"""
Function that computes the closest point on the mesh for an array of points. Search is done linearly.
args:
    points: array of points
    triangles: list of triangle objects
return:
    array of closest points on mesh
"""
def find_closest_point_on_mesh(points, triangles):

    closest_points = []
    for i in range(points.shape[0]):
        min_dist = np.inf
        closest_point = None

        for t in triangles:
            p = t.closest_point(points[i])
            dist = np.sum(np.square(p - points[i]))

            if dist < min_dist:
                min_dist = dist
                closest_point = p

        closest_points.append(closest_point)
    
    closest_points = np.array(closest_points)
    return closest_points

"""
Function that constructs a list of triangle objects.
args:
    vertices: array of vertices of mesh
    triangle_inds: array of where vertices of triangle indices are located.
return:
    list of triangle objects.
"""
def build_triangles(vertices, triangle_inds):

    triangles = []
    for i in range(triangle_inds.shape[0]):
        p1 = vertices[triangle_inds[i, 0]]
        p2 = vertices[triangle_inds[i, 1]]
        p3 = vertices[triangle_inds[i, 2]]

        triangles.append(tg.triangle(p1, p2, p3))
    return triangles

"""
Function that computes the location of the pointer tip with respect to rigid body B
This is done by registering each frame of the bodies to the tracker to compute FA and FB, then the location of the pointer is simply FB.inv(FA.dot(tip))

args:
    Abodyfile: the file containing rigid body A
    Abodyfile: the file containing rigid body B
    samplefile: the file containing sample readings
return:
    dk: The location of the tip w.r.t. rigid body B
"""
def compute_pointer_tip(Abodyfile, Bbodyfile, samplefile):
    Na, A_LED, A_tip = IO.read_body_file(Abodyfile)
    Nb, B_LED, B_tip = IO.read_body_file(Bbodyfile)
    A_tracker, B_tracker, D_tracker = IO.read_sample_readings(samplefile, Na, Nb)

    # Compute FA,k and FB,k
    FA, FB = [], []
    for k in range(A_tracker.shape[0]):
        FA.append(tf.register(A_LED, A_tracker[k]))
        FB.append(tf.register(B_LED, B_tracker[k]))

    # Compute dk
    dk = []
    for k in range(A_tracker.shape[0]):
        dk.append(FB[k].inv(FA[k].dot(A_tip)))
    
    dk = np.array(dk)
    return dk

def main(argv):

    Abodyfile = argv[0]
    Bbodyfile = argv[1]
    meshfile = argv[2]
    samplefile = argv[3]
    #outfile = argv[4]

    vertices, triangle_inds = IO.read_mesh_file(meshfile)
    #d, c, dist = IO.read_output(outfile)

    # List of triangle objects
    triangles = build_triangles(vertices, triangle_inds)    

    # Compute dk
    dk = compute_pointer_tip(Abodyfile, Bbodyfile, samplefile)

    # For purposes of problem 3, sample points sk = dk
    sk = dk

    # Compute closest points on mesh
    ck = find_closest_point_on_mesh(sk, triangles)

    # Compute distances between dk and ck
    dist = np.sqrt(np.sum(np.square(dk - ck), axis=1))
   
    """
    Compute distances between output ck and debug ck
    error = np.sum(np.square(c - ck), axis = 1)
    errornum = np.sum(error) / ck.shape[0]
    print(errornum)
    """

    # Generate output file
    outputfn = "OUTPUT/" + outfile.split("/")[1]
    IO.print_output(outputfn, ck.shape[0], np.round(dk, 2), np.round(ck, 2), np.round(dist, 3))

if __name__=="__main__":
    main(sys.argv[1:])
