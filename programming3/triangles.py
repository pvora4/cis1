import sys
import numpy as np

"""
A class for Triangles
Args: _p1, _p2, _p3 are all 3x1 arrays representing vertices of the triangle.

"""
class triangle:
    def __init__(self, _p1, _p2, _p3):
       
        if (_p1 == _p2).all() or (_p1 == _p3).all() or (_p2 == _p3).all():
            raise Exception("Degenerate triangle: Overlapping vertices") 

        # points making up triangle
        self.p1 = _p1
        self.p2 = _p2
        self.p3 = _p3

        # basis vectors
        self.v1 = _p2 - _p1
        self.v2 = _p3 - _p1
        self.v3 = _p3 - _p2

        # Norm vector to plane
        self.norm = np.cross(self.v2, self.v1)
        self.norm_mag = np.linalg.norm(self.norm)
        if self.norm_mag == 0:
            raise Exception("Degenerate triangle: a line")
    
    """
    A function computing the projection of a point onto an edge.
    Args: start, direction, point.
    Return: projection of point onto edge.
    """
    def closest_point_on_edge(self, start, direction, point):
        normdir = direction / np.linalg.norm(direction)
        d = np.dot((point - start), normdir)
        return start + normdir * d

    """
    A function computing the closest point on the triangle to some other point.
    Args: original point
    Return: closest point on triangle
    """
    def closest_point(self, p_orig):

        # Convert to barycentric coordinates using least squares
        A = np.zeros((3,2))
        A[:,0] = self.v1
        A[:,1] = self.v2
        b = p_orig - self.p1
        x = np.linalg.lstsq(A, b, rcond=None)[0]
       
        # u, v, w are the barycentric coords
        u, v = x[0], x[1]
        w = 1 - u - v

        # If these conditions are met, we are in the triangle
        if u >= 0 and v >= 0 and u + v <= 1:
            p = self.p1 + u * (self.p2 - self.p1) + v * (self.p3 - self.p1)
            return p

        # Somewhere on p1-p3
        if u <= 0:

            # Closest point is a vertex
            if v <= 0:
                return self.p1
            if w <= 0:
                return self.p3
            
            # return projection of p onto p1-p3
            proj = self.closest_point_on_edge(self.p1, self.v2, p_orig)
            return proj

        # Somewhere on p1-p2
        elif v <= 0:

            # Closest point is a vertex
            if w <= 0:
                return self.p2

            # return projection of p onto p1-p2
            proj = self.closest_point_on_edge(self.p1, self.v1, p_orig)
            return proj

        # return projection of p onto p2-p3
        proj = self.closest_point_on_edge(self.p2, self.v3, p_orig)
        return proj

