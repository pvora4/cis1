"""
Parth Vora & Miguel Inserni
CIS I PA3
11/21/2019
"""

import numpy as np
import sys

import IO
import transformations as tf
import triangles as tg
import bounding_spheres as bsph
import bounded_octree as boct

"""
Function that computes the location of the pointer tip with respect to rigid body B
This is done by registering each frame of the bodies to the tracker to compute FA and FB, then the location of the pointer is simply FB.inv(FA.dot(tip))

args:
    Abodyfile: the file containing rigid body A
    Abodyfile: the file containing rigid body B
    samplefile: the file containing sample readings
return:
    dk: The location of the tip w.r.t. rigid body B
"""
def compute_pointer_tip(Abodyfile, Bbodyfile, samplefile):
    Na, A_LED, A_tip = IO.read_body_file(Abodyfile)
    Nb, B_LED, B_tip = IO.read_body_file(Bbodyfile)
    A_tracker, B_tracker, D_tracker = IO.read_sample_readings(samplefile, Na, Nb)

    # Compute FA,k and FB,k
    FA, FB = [], []
    for k in range(A_tracker.shape[0]):
        FA.append(tf.register(A_LED, A_tracker[k]))
        FB.append(tf.register(B_LED, B_tracker[k]))

    # Compute dk
    dk = []
    for k in range(A_tracker.shape[0]):
        dk.append(FB[k].inv(FA[k].dot(A_tip)))
    
    dk = np.array(dk)
    return dk

"""
Function that constructs bounding sphere objects for all triangle vertices.

args:
    array of vertices
    array of triangle inds
returns:
    list of sphere objects
"""
def build_spheres(vertices, triangle_inds):

    spheres = []
    for i in range(triangle_inds.shape[0]):
        p1 = vertices[triangle_inds[i, 0]]
        p2 = vertices[triangle_inds[i, 1]]
        p3 = vertices[triangle_inds[i, 2]]

        spheres.append(bsph.sphere(p1, p2, p3))
    return spheres

"""
Function performing iterative closest point algorithm to register pointer tips to the mesh.

args:
    dk: array of pointer tip locations
    octree of bounded spheres
returns:
    Freg transformation
    sk points
    ck points
"""
def ICP(dk, octree):

    # Initialize Freg to identity
    Freg = tf.transform(np.identity(3), np.zeros(3))
    
    # ICP parameters
    converged = False
    dist_threshold = 1
    stop_threshold = 0.001 # Average error is < 0.001mm
    max_epochs = 100

    epochs = 0
    while not converged:
        
        w = np.zeros(dk.shape[0])
        sk = np.zeros(dk.shape, dtype=float)
        ck = np.zeros(dk.shape, dtype=float)
        for i in range(dk.shape[0]):

            # Apply Freg to compute sample points
            sk[i] = Freg.dot(dk[i])

            # Compute closest points on mesh to sample points
            ck[i], bound = octree.search(octree.root, sk[i])

            # If dist is within a threshold, we keep it.
            dist = np.linalg.norm(ck[i] - sk[i])
            if dist < dist_threshold:
                w[i] = 1

        # Get inlier points
        dk_inliers = dk[np.where(w == 1)]
        ck_inliers = ck[np.where(w == 1)]

        # Reestimate Freg on inliers by registering dk to ck
        Fprev = Freg
        Freg = tf.register(dk_inliers, ck_inliers)
        
        # Evaluate converged condition
        diff = np.sqrt(np.sum(np.square(sk - ck))) / sk.shape[0]
        #print(diff)
        if diff < stop_threshold or epochs == max_epochs:
            converged = True
    
        epochs += 1

    return Freg, sk, ck

def main(argv):

    Abodyfile = argv[0]
    Bbodyfile = argv[1]
    meshfile = argv[2]
    samplefile = argv[3]
    outfile = argv[4]

    vertices, triangle_inds = IO.read_mesh_file(meshfile)
    #d_out, c_out, dist = IO.read_output(outfile)

    # Construct sphere objects & organize into octree
    spheres = build_spheres(vertices, triangle_inds)
    octree = boct.octree(spheres)

    # Compute dk
    dk = compute_pointer_tip(Abodyfile, Bbodyfile, samplefile)
    
    # Compute Freg
    Freg, sk, ck = ICP(dk, octree)

    # Compute distances
    dist = np.sqrt(np.sum(np.square(sk - ck), axis=1))

    # Generate output file
    outputfn = outfile.split("/")[1]
    IO.print_output(outputfn, ck.shape[0], np.round(sk, 2), np.round(ck, 2), np.round(dist, 3))

if __name__=="__main__":
    main(sys.argv[1:])
