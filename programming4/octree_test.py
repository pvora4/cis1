#!/bin/python3 

import numpy as np
import sys

import bounded_octree as boct
import bounding_spheres as bsph
import triangles as tg

def search_works():

    spheres = []
    for i in range(500):
        p1 = np.random.rand(3)
        p2 = np.random.rand(3)
        p3 = np.random.rand(3)

        spheres.append(bsph.sphere(p1, p2, p3))

    octree = boct.octree(spheres)
    for s in spheres:
        p, bound = octree.search(octree.root, s.a)
        
        sys.exit(0)
        assert(np.array_equal(np.round(p, 5), np.round(s.a, 5)))

# Four spheres with centroid at (0, 0, 0)
def centroid_calculation():
    
    # s1 has center (0.5, 0.5, 0)
    p1 = np.array((0, 0, 0))
    p2 = np.array((1, 0, 0))
    p3 = np.array((0, 1, 0))
    s1 = bsph.sphere(p1, p2, p3)

    # s2 has center (-0.5, 0.5, 0)
    p4 = np.array((0, 0, 0))
    p5 = np.array((-1, 0, 0))
    p6 = np.array((0, 1, 0))
    s2 = bsph.sphere(p4, p5, p6)

    # s3 has center (0.5, -0.5, 0)
    p7 = np.array((0, 0, 0))
    p8 = np.array((1, 0, 0))
    p9 = np.array((0, -1, 0))
    s3 = bsph.sphere(p7, p8, p9)

    # s4 has center (-0.5, -0.5, 0)
    p10 = np.array((0, 0, 0))
    p11 = np.array((-1, 0, 0))
    p12 = np.array((0, -1, 0))
    s4 = bsph.sphere(p10, p11, p12)

    on = boct.octree.octree_node([s1, s2, s3, s4])
    assert(np.array_equal(np.zeros(3), on.center))

def main():

    centroid_calculation()
    search_works()

if __name__ == "__main__":
    main()
