#!/bin/bash

ABODYFILE="data/Problem3-BodyA.txt"
BBODYFILE="data/Problem3-BodyB.txt"
MESHFILE="data/Problem3MeshFile.sur"
SAMPLEFILE="data/PA3-A-Debug-SampleReadingsTest.txt"
OUTFILE="data/PA3-A-Debug-Output.txt"

python3 main.py $ABODYFILE $BBODYFILE $MESHFILE $SAMPLEFILE $OUTFILE
